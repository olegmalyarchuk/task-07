package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.mysql.cj.Messages;

import javax.sql.DataSource;


public class DBManager {

//	public static final String CONNECTION = "jdbc:mysql://10.7.0.9:3307/?user=testuser&password=testpass";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance==null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {

	}

	public static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
	public static final String SQL_CREATE_USER = "INSERT INTO users (login) VALUES (?)";
	public static final String SQL_GET_USER_ID = "SELECT id FROM users WHERE login = ?";
	public static final String SQL_GET_TEAM_ID = "SELECT id FROM teams WHERE name = ?";
	public static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
	public static final String SQL_CREATE_TEAM = "INSERT INTO teams(name) VALUES(?)";
	public static final String SQL_SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES(?, ?)";
	public static final String SQL_GET_USER_TEAMS = "SELECT * FROM users_teams WHERE user_id=?";
	public static final String SQL_GET_TEAM = "SELECT * FROM teams WHERE id=?";
	public static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
	public static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
	public static final String SQL_DELETE_USER = "DELETE FROM users WHERE login=?";
	public static final String SQL_DELETE_USER_TEAMS_BY_USERID = "DELETE FROM users_teams WHERE user_id=?";
	public static final String SQL_DELETE_USER_TEAMS_BY_TEAMID = "DELETE FROM users_teams WHERE team_id=?";

	public List<User> findAllUsers() throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			 connection = getConnection();
			preparedStatement = connection.prepareStatement(SQL_FIND_ALL_USERS);
			resultSet = preparedStatement.executeQuery();
			List<User> userList = new ArrayList<>();
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String login = resultSet.getString(2);
				User u = new User();
				u.setId(id);
				u.setLogin(login);
				userList.add(u);
			}
			return userList;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(connection, preparedStatement, resultSet);
		}
		return new ArrayList<>();
	}

	public boolean insertUser(User user) throws DBException {
		List<User> userList = findAllUsers();
		boolean has = false;
		if(userList!=null) {
			has = userList.contains(user);
		}
		if(has) return false;
		else {
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			try {
				 connection = getConnection();
				 preparedStatement = connection.prepareStatement(SQL_CREATE_USER);
				 preparedStatement.setString(1, user.getLogin());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(connection);
				close(preparedStatement);
			}
			return true;
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			 preparedStatement = connection.prepareStatement(SQL_DELETE_USER);
			for(int i = 0; i < users.length; i++) {
				User u = users[i];
				preparedStatement.setString(1, u.getLogin());
				preparedStatement.executeUpdate();
				PreparedStatement preparedStatement1 = connection.prepareStatement(SQL_DELETE_USER_TEAMS_BY_USERID);
				int id = u.getId();
				preparedStatement1.setInt(1, id);
				preparedStatement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(connection);
			close(preparedStatement);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			 preparedStatement = connection.prepareStatement(SQL_GET_USER_ID);
			preparedStatement.setString(1, login);
			 resultSet = preparedStatement.executeQuery();
			int id = 0;
			while (resultSet.next()) {
				id = resultSet.getInt("id");
			}

			User u = new User();
			u.setId(id);
			u.setLogin(login);
			return u;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			close(connection, preparedStatement, resultSet);
		}
	}

	public Team getTeam(String name) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			 preparedStatement = connection.prepareStatement(SQL_GET_TEAM_ID);
			preparedStatement.setString(1, name);
			 resultSet = preparedStatement.executeQuery();
			int id = 0;
			while (resultSet.next()) {
				id = resultSet.getInt("id");
			}

			Team t = new Team();
			t.setId(id);
			t.setName(name);
			return t;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			close(connection, preparedStatement, resultSet);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			 preparedStatement = connection.prepareStatement(SQL_FIND_ALL_TEAMS);
			 resultSet = preparedStatement.executeQuery();
			List<Team> teamList = new ArrayList<>();
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				Team t = new Team();
				t.setId(id);
				t.setName(name);
				teamList.add(t);
			}
			return teamList;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(connection, preparedStatement, resultSet);
		}
		return new ArrayList<>();
	}

	public boolean insertTeam(Team team) throws DBException {
//		List<Team> teamList = findAllTeams();
//		boolean has = false;
//		if(teamList != null) {
//			has = teamList.contains(team);
//		}
//		if(has) return false;
//		else {
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			try {
				connection = getConnection();
				 preparedStatement = connection.prepareStatement(SQL_CREATE_TEAM);
				preparedStatement.setString(1, team.getName());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(connection);
				close(preparedStatement);
			}
			return true;
	//	}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(SQL_SET_TEAMS_FOR_USER);
			for(int i = 0; i < teams.length; i++) {
				Team t = teams[i];

				int userId = getUser(user.getLogin()).getId();
				int teamsId = getTeam(t.getName()).getId();

				preparedStatement.setInt(1, userId);
				preparedStatement.setInt(2, teamsId);
				preparedStatement.executeUpdate();
			}
			connection.commit();
		} catch (SQLException e) {
			rollback(connection);
			throw new DBException("failed", e);
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			close(connection);
			close(preparedStatement);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {

			connection = getConnection();
			 preparedStatement = connection.prepareStatement(SQL_GET_USER_TEAMS);
			int userId = getUser(user.getLogin()).getId();
			preparedStatement.setInt(1, userId);
			 resultSet = preparedStatement.executeQuery();
			List<Integer> listTeamId = new ArrayList<>();
			List<Team> teamList = new ArrayList<>();
			while (resultSet.next()) {
				listTeamId.add(resultSet.getInt(2));
			}

			for(int i = 0; i < listTeamId.size(); i++) {
				int id;
				String name;
				preparedStatement = connection.prepareStatement(SQL_GET_TEAM);
				preparedStatement.setInt(1, listTeamId.get(i));
				resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
					id = resultSet.getInt("id");
					name = resultSet.getString("name");
					Team t = new Team();
					t.setId(id);
					t.setName(name);
					teamList.add(t);
				}
			}
			return teamList;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(connection, preparedStatement, resultSet);
		}
		return null;	
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			  preparedStatement= connection.prepareStatement(SQL_DELETE_TEAM);
			preparedStatement.setString(1, team.getName());
			preparedStatement.executeUpdate();
			PreparedStatement preparedStatement1 = connection.prepareStatement(SQL_DELETE_USER_TEAMS_BY_TEAMID);
			int id = getTeam(team.getName()).getId();
			preparedStatement1.setInt(1, id);
			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(connection);
			close(preparedStatement);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = getConnection();
			 preparedStatement = connection.prepareStatement(SQL_UPDATE_TEAM);
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, getTeam(team.getOldValue()).getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(connection);
			close(preparedStatement);
		}
		return true;
	}

	public static Connection getConnection() throws SQLException {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("app.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String url = properties.getProperty("connection.url");

		return DriverManager.getConnection(url);
	}

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException ex) {
			}
		}
	}

	private void close(PreparedStatement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException ex) {
			}
		}
	}

	private void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {

			}
		}
	}

	private void close(Connection con, PreparedStatement stmt, ResultSet rs) {
		close(rs);
		close(stmt);
		close(con);
	}

	private void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException ex) {
			}
		}
	}

}

