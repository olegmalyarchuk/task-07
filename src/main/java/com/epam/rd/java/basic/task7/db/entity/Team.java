package com.epam.rd.java.basic.task7.db.entity;

import java.sql.*;
import java.util.Objects;

public class Team {

	private int id;

	private String name;
	private String oldValue;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getOldValue() { return oldValue; }

	public void setName(String name) {
		if(this.name==null) {
			this.name = name;
		} else {
			this.oldValue = this.name;
			this.name = name;
		}
	}

	public static Team createTeam(String name) {
			Team t = new Team();
			t.setName(name);
			return t;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Team team = (Team) o;
		return Objects.equals(name, team.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public String toString() {
		return ""+name+"";
	}

}
